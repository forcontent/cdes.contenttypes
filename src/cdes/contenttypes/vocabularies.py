# -*- coding: utf-8 -*-
from cdes.contenttypes.interfaces import ICDESSettings
from plone.i18n.normalizer.interfaces import IIDNormalizer
from plone.registry.interfaces import IRegistry
from zope.component import getUtility
from zope.schema.vocabulary import SimpleVocabulary


def ActivitiesVocabulary(context):
    """Activities vocabulary."""
    normalizer = getUtility(IIDNormalizer)
    registry = getUtility(IRegistry)
    settings = registry.forInterface(ICDESSettings)
    items = []
    for activity in settings.available_activities:
        token = normalizer.normalize(activity)
        items.append(SimpleVocabulary.createTerm(token, token, activity))
    return SimpleVocabulary(items)


def DocumentTypesVocabulary(context):
    """Document types vocabulary."""
    normalizer = getUtility(IIDNormalizer)
    registry = getUtility(IRegistry)
    settings = registry.forInterface(ICDESSettings)
    items = []
    for document_type in settings.available_document_types:
        token = normalizer.normalize(document_type)
        items.append(SimpleVocabulary.createTerm(token, token, document_type))
    return SimpleVocabulary(items)
