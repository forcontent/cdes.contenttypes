# -*- coding: utf-8 -*-
PROJECTNAME = 'cdes.contenttypes'

DEFAULT_ACTIVITIES = [
    u'Reunião Plenária',
    u'Reunião de Grupo de Trabalho',
    u'Reunião do Comitê Gestor',
    u'Seminário ou outro evento',
    u'Observatório da equidade',
    u'AICESIS',
    u'CESALC',
]

DEFAULT_DOCUMENT_TYPES = [
    u'Atas',
    u'Publicações',
    u'Legislação',
    u'Memória de reunião',
    u'Deliberação',
    u'Relatório de atividades',
    u'Outros',
]
