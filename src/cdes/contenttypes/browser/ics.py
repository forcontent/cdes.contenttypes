# -*- coding: utf-8 -*-
from cdes.contenttypes.config import PROJECTNAME
from cStringIO import StringIO
from DateTime import DateTime
from plone.uuid.interfaces import IUUID
from Products.ATContentTypes.lib.calendarsupport import foldLine
from Products.ATContentTypes.lib.calendarsupport import n2rn
from Products.ATContentTypes.lib.calendarsupport import vformat
from Products.Five import BrowserView


# iCal header and footer
ICS_HEADER = """\
BEGIN:VCALENDAR
PRODID:%(prodid)s
VERSION:2.0
METHOD:PUBLISH
"""

ICS_FOOTER = """\
END:VCALENDAR
"""

# iCal event
ICS_EVENT_START = """\
BEGIN:VEVENT
DTSTAMP:%(dtstamp)s
CREATED:%(created)s
UID:ATEvent-%(uid)s
LAST-MODIFIED:%(modified)s
SUMMARY:%(summary)s
DTSTART:%(startdate)s
DTEND:%(enddate)s
"""

ICS_EVENT_END = """\
CLASS:PUBLIC
END:VEVENT
"""


class ICSView (BrowserView):
    """View ICal. Temporary, remove after update plone.app.contenttypes"""

    icalformat = '%Y%m%dT%H%M%SZ'

    def getical(self):
        """get iCal data"""
        context = self.context
        out = StringIO()
        data = {
            'dtstamp': DateTime().strftime(self.icalformat),
            'created': DateTime(context.CreationDate()).strftime(self.icalformat),
            'uid': IUUID(context),
            'modified': DateTime(context.ModificationDate()).strftime(self.icalformat),
            'summary': vformat(context.Title()),
            'startdate': context.start_date.strftime(self.icalformat),
            'enddate': context.end_date.strftime(self.icalformat),
        }
        out.write(ICS_EVENT_START % data)

        description = context.Description()
        if description:
            out.write(foldLine('DESCRIPTION:%s\n' % vformat(description)))

        location = context.location
        if location:
            location = location.encode('utf-8')
            out.write('LOCATION:%s\n' % vformat(location))

        cn = []
        contact = context.contact_name
        if contact:
            cn.append(contact.encode('utf-8'))
        if cn:
            out.write('CONTACT:%s\n' % vformat(', '.join(cn)))

        url = context.absolute_url()
        if url:
            out.write('URL:%s\n' % url)

        out.write(ICS_EVENT_END)
        return out.getvalue()

    def __call__(self):
        """vCalendar output"""
        response = self.request.response
        response.setHeader('Content-Type', 'text/calendar')
        response.setHeader('Content-Disposition',
                           'attachment; filename="%s.ics"' % self.context.getId())
        out = StringIO()
        out.write(ICS_HEADER % {'prodid': PROJECTNAME})
        out.write(self.getical())
        out.write(ICS_FOOTER)
        return n2rn(out.getvalue())
