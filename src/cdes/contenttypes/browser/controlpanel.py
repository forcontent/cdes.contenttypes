# -*- coding: utf-8 -*-
from cdes.contenttypes import _
from cdes.contenttypes.interfaces import ICDESSettings
from plone.app.registry.browser import controlpanel


class CDESSettingsEditForm(controlpanel.RegistryEditForm):
    schema = ICDESSettings
    label = _(u'CDES Settings')
    description = _(u'Here you can modify the settings for cdes.contenttypes.')


class CDESSettingsControlPanel(controlpanel.ControlPanelFormWrapper):
    form = CDESSettingsEditForm
