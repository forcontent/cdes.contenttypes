# -*- coding: utf-8 -*-
from plone.dexterity.browser.view import DefaultView


class View(DefaultView):
    """Default view of a Counselor."""
