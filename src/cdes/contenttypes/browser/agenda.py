# -*- coding: utf-8 -*-
from cdes.contenttypes import _
from DateTime import DateTime
from DateTime.interfaces import DateTimeError
from plone import api
from Products.Five import BrowserView
from zope.component import getMultiAdapter
from zope.i18nmessageid import Message


class Agenda(BrowserView):

    def get_events(self, date_agenda=None, search_text=None):
        """Return results in catalog by date or search text or all events."""

        if date_agenda:
            return self.get_event_date(date_agenda)
        elif search_text:
            return self.get_search_text(search_text)
        else:
            return self.get_events_list()

    def get_events_list(self):
        """Return all events sort by date in order reverse"""

        result = api.content.find(portal_type='Event',
                                  path='/'.join(self.context.getPhysicalPath()),
                                  sort_on='start',
                                  sort_order='reverse')

        return self._get_list(result)

    def get_event_date(self, date_agenda):
        """Return all the events that started on this day and Ends on this day."""

        try:
            start = DateTime(date_agenda + ' 00:00:00')
            end = DateTime(date_agenda + ' 23:59:59')
            result = api.content.find(portal_type='Event',
                                      path='/'.join(self.context.getPhysicalPath()),
                                      start={'query': end, 'range': 'max'},
                                      end={'query': start, 'range': 'min'},
                                      sort_on='start')
        except DateTimeError:
            result = []

        return self._get_list(result)

    def get_search_text(self, text):
        """Return all events contains the search term."""

        result = api.content.find(portal_type='Event',
                                  path='/'.join(self.context.getPhysicalPath()),
                                  SearchableText=text,
                                  sort_on='start')

        return self._get_list(result)

    def _translate(self, msgid, locale='plonelocales', mapping=None):
        """Return message for translate."""

        tool = api.portal.get_tool(name='translation_service')
        portal_state = getMultiAdapter((self.context, self.request),
                                       name=u'plone_portal_state')
        # Fix language pt-br to pt_BR
        current_language = portal_state.language()
        target_language = ('pt_BR' if current_language == 'pt-br' else current_language)

        return tool.translate(msgid,
                              locale,
                              mapping=mapping,
                              context=self.context,
                              target_language=target_language)

    def long_date(self, date):
        """Return the day, weekday, month and  year to display in template."""

        try:
            ts = api.portal.get_tool(name='translation_service')
            date = DateTime(date)

            parts = dict(weekday=self._translate(ts.day_msgid(date.strftime('%w'))),
                         day=date.strftime('%d'),
                         month=self._translate(ts.month_msgid(date.strftime('%m'))),
                         year=date.strftime('%Y'))
            ldm = self._translate(Message(_(u'long_date_agenda'),
                                          mapping=parts))

        except DateTimeError:
            ldm = ''

        return ldm

    def _get_list(self, brains=[]):
        """Return a dictionary over catalog results."""

        ts = api.portal.get_tool(name='translation_service')
        events = []

        for brain in brains:

            obj = brain.getObject()
            start_date = obj.start_date
            title = brain.Title
            alt = getattr(obj, 'image_description', title)
            event_type = getattr(obj, 'event_type', '')
            attendees = getattr(obj, 'attendees', '')
            attendees_formatted = attendees and '<br/>'.join(attendees.split('\n')) or ''
            text = obj.text and obj.text.output or ''

            image_tag = None
            if getattr(obj, 'image'):
                scale = obj.restrictedTraverse('@@images')
                image_tag = scale.scale('image', scale='large').tag(css_class='eventImage', title=alt, alt=alt)
            image_tag = image_tag

            time = start_date.strftime('%Hh%M')
            day = start_date.strftime('%d')
            month = str(self._translate(ts.month(start_date.strftime('%m'),
                                                 format='a'))).upper()

            events.append(dict(attendees=attendees,
                               attendees_formatted=attendees_formatted,
                               description=brain.Description,
                               review_state=brain.review_state,
                               title=title,
                               event_type=event_type,
                               start_time=time,
                               start_date=day,
                               month=month,
                               location=obj.location,
                               text=text,
                               image_tag=image_tag,
                               url=obj.absolute_url()
                               ))

        return events
