# -*- coding: utf-8 -*-
from cdes.contenttypes import _
from cdes.contenttypes.config import DEFAULT_ACTIVITIES
from cdes.contenttypes.config import DEFAULT_DOCUMENT_TYPES
from plone.app.textfield import RichText
from plone.autoform import directives as form
from plone.namedfile.field import NamedBlobImage
from plone.supermodel import model
from zope import schema
from zope.interface import Interface


class IBrowserLayer(Interface):
    """A layer specific for this add-on product."""


class ICounselor(model.Schema):
    """A counselor."""

    given_name = schema.TextLine(
        title=_(u'Given name'),
        description=_(u'First name of the counselor.'),
        required=True,
        default=u'',
    )

    surname = schema.TextLine(
        title=_(u'Surname'),
        description=_(u'Last name of the counselor.'),
        required=True,
        default=u'',
    )

    text = RichText(
        title=_(u'Biography'),
        required=False,
    )

    image = NamedBlobImage(
        title=_(u'Portrait'),
        required=False,
    )


class IAgenda(Interface):
    """ A Meeting Agenda """


class ICDESSettings(model.Schema):
    """Interface for the control panel form."""

    form.widget('available_activities', rows=7)
    available_activities = schema.List(
        title=_(u'Available Activities'),
        description=_(u'List of available activities in the site.'),
        required=True,
        default=DEFAULT_ACTIVITIES,
        value_type=schema.TextLine(title=_(u'Activity')),
    )

    form.widget('available_document_types', rows=7)
    available_document_types = schema.List(
        title=_(u'Available Document Types'),
        description=_(u'List of available document types in the site.'),
        required=True,
        default=DEFAULT_DOCUMENT_TYPES,
        value_type=schema.TextLine(title=_(u'Document Type')),
    )
