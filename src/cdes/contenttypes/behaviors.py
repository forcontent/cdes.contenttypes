# -*- coding: utf-8 -*-
from cdes.contenttypes import _
from plone.autoform.interfaces import IFormFieldProvider
from plone.namedfile import field as namedfile
from plone.supermodel import model
from zope import schema
from zope.interface import provider


@provider(IFormFieldProvider)
class IDocumentClassification(model.Schema):
    """Document classification."""

    activity = schema.Choice(
        title=_(u'Activity'),
        vocabulary='cdes.contenttypes.Activities',
        required=True,
    )

    document_type = schema.Choice(
        title=_(u'Document Type'),
        vocabulary='cdes.contenttypes.DocumentTypes',
        required=True,
    )


@provider(IFormFieldProvider)
class IExtraFieldEvent(model.Schema):
    """Fields for event."""

    event_type = schema.TextLine(
        title=_(u'Event Type'),
        required=False
    )

    image = namedfile.NamedBlobImage(
        title=_(u'Lead Image'),
        description=u'',
        required=False,
    )

    image_description = schema.TextLine(
        title=_(u'Image Description'),
        description=u'',
        required=False,
    )
