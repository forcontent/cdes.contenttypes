# -*- coding: utf-8 -*-
from cdes.contenttypes.interfaces import IAgenda
from cdes.contenttypes.interfaces import ICounselor
from plone.dexterity.content import Container
from plone.dexterity.content import Item
from zope.interface import implementer


@implementer(ICounselor)
class Counselor(Item):
    """A Counselor."""

    @property
    def title(self):
        return self.fullname

    @title.setter
    def title(self, value):
        pass

    @property
    def description(self):
        return u''

    @description.setter
    def description(self, value):
        pass

    @property
    def fullname(self):
        """Return the fullname of a counselor."""
        return self.given_name + ' ' + self.surname


@implementer(IAgenda)
class Agenda(Container):
    """ A  Meeting Agenda """
