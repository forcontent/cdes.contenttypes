# -*- coding: utf-8 -*-
from cdes.contenttypes.interfaces import ICounselor
from plone.app.textfield.interfaces import ITransformer
from plone.indexer.decorator import indexer
from Products.CMFPlone.utils import safe_unicode


@indexer(ICounselor)
def SearchableText(obj):
    """SearchableText contains id, title, body text and keywords."""
    transformer = ITransformer(obj)
    text = transformer(obj.text, 'text/plain')

    subject = u' '.join(
        [safe_unicode(s) for s in obj.Subject()])

    return u' '.join((
        safe_unicode(obj.id),
        safe_unicode(obj.title) or u'',
        safe_unicode(text),
        safe_unicode(subject),
    ))
