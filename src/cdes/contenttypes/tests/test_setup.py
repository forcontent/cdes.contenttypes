# -*- coding: utf-8 -*-
from cdes.contenttypes.config import PROJECTNAME
from cdes.contenttypes.interfaces import IBrowserLayer
from cdes.contenttypes.testing import INTEGRATION_TESTING
from plone import api
from plone.browserlayer.utils import registered_layers

import unittest


class InstallTestCase(unittest.TestCase):
    """Ensure product is properly installed."""

    layer = INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.qi = self.portal['portal_quickinstaller']

    def test_installed(self):
        self.assertTrue(self.qi.isProductInstalled(PROJECTNAME))

    def test_browser_layer_installed(self):
        self.assertIn(IBrowserLayer, registered_layers())

    def test_add_permission(self):
        permission = 'cdes.contenttypes: Add Counselor'
        roles = self.portal.rolesOfPermission(permission)
        roles = [r['name'] for r in roles if r['selected']]
        expected = ['Contributor', 'Manager', 'Owner', 'Site Administrator']
        self.assertEqual(roles, expected)

    def test_add_agenda_permission(self):
        permission = 'cdes.contenttypes: Add Agenda'
        portal = self.portal
        allowed = [x['name']
                   for x in portal.rolesOfPermission(permission)
                   if x['selected']]
        self.assertEqual(allowed,
                         ['Contributor', 'Manager', 'Owner', 'Site Administrator'])

    def test_setup_permission(self):
        permission = 'cdes.contenttypes: Setup'
        roles = self.portal.rolesOfPermission(permission)
        roles = [r['name'] for r in roles if r['selected']]
        expected = ['Manager', 'Site Administrator']
        self.assertListEqual(roles, expected)


class UninstallTest(unittest.TestCase):
    """Ensure product is properly uninstalled."""

    layer = INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.qi = self.portal['portal_quickinstaller']

        with api.env.adopt_roles(['Manager']):
            self.qi.uninstallProducts(products=[PROJECTNAME])

    def test_uninstalled(self):
        self.assertFalse(self.qi.isProductInstalled(PROJECTNAME))

    def test_browser_layer_removed_uninstalled(self):
        self.assertNotIn(IBrowserLayer, registered_layers())
