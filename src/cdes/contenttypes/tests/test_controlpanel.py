# -*- coding: utf-8 -*-
from cdes.contenttypes.config import DEFAULT_ACTIVITIES
from cdes.contenttypes.config import DEFAULT_DOCUMENT_TYPES
from cdes.contenttypes.config import PROJECTNAME
from cdes.contenttypes.interfaces import ICDESSettings
from cdes.contenttypes.testing import INTEGRATION_TESTING
from plone import api
from plone.app.testing import logout
from plone.registry.interfaces import IRegistry
from zope.component import getUtility

import unittest


class ControlPanelTestCase(unittest.TestCase):

    layer = INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.controlpanel = self.portal['portal_controlpanel']

    def test_controlpanel_has_view(self):
        request = self.layer['request']
        view = api.content.get_view(u'cdes-settings', self.portal, request)
        view = view.__of__(self.portal)
        self.assertTrue(view())

    def test_controlpanel_view_is_protected(self):
        from AccessControl import Unauthorized
        logout()
        with self.assertRaises(Unauthorized):
            self.portal.restrictedTraverse('@@cdes-settings')

    def test_controlpanel_installed(self):
        actions = [a.getAction(self)['id']
                   for a in self.controlpanel.listActions()]
        self.assertIn('cdes', actions, 'configlet not installed')

    def test_controlpanel_removed_on_uninstall(self):
        qi = self.portal['portal_quickinstaller']

        with api.env.adopt_roles(['Manager']):
            qi.uninstallProducts(products=[PROJECTNAME])

        actions = [a.getAction(self)['id']
                   for a in self.controlpanel.listActions()]
        self.assertNotIn('cdes', actions, 'configlet not removed')


class RegistryTestCase(unittest.TestCase):

    layer = INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.registry = getUtility(IRegistry)
        self.settings = self.registry.forInterface(ICDESSettings)

    def test_available_activities_record_in_registry(self):
        self.assertTrue(hasattr(self.settings, 'available_activities'))
        self.assertEqual(self.settings.available_activities, DEFAULT_ACTIVITIES)

    def test_available_document_types_record_in_registry(self):
        self.assertTrue(hasattr(self.settings, 'available_document_types'))
        self.assertEqual(self.settings.available_document_types, DEFAULT_DOCUMENT_TYPES)

    def test_records_removed_on_uninstall(self):
        qi = self.portal['portal_quickinstaller']

        with api.env.adopt_roles(['Manager']):
            qi.uninstallProducts(products=[PROJECTNAME])

        BASE_REGISTRY = ICDESSettings.__identifier__
        records = [
            BASE_REGISTRY + '.available_activities',
            BASE_REGISTRY + '.available_document_types',
        ]

        for r in records:
            self.assertNotIn(r, self.registry)
