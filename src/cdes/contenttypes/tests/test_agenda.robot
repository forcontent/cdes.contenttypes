*** Settings ***

Resource  plone/app/robotframework/keywords.robot
Variables  plone/app/testing/interfaces.py
Library  Remote  ${PLONE_URL}/RobotRemote

Test Setup  Open Test Browser
Test Teardown  Close All Browsers

*** Variables ***

${title_selector} =  input#form-widgets-IDublinCore-title
${description_selector} =  textarea#form-widgets-IDublinCore-description

*** Test cases ***

Test CRUD
    Enable Autologin as  Site Administrator
    Goto Homepage

    Create
    Update
    Delete

*** Keywords ***

Click Add Meeting Agenda
    Open Add New Menu
    Click Link  css=a#cdes-agenda
    Page Should Contain  Add Meeting Agenda

Create
    Click Add Meeting Agenda
    Input Text  css=${title_selector}  Agenda
    Input Text  css=${description_selector}  A agenda
    Click Button  Save
    Page Should Contain  Item created
    Page Should Contain  Agenda

Update
    [arguments]

    Click Link  link=Edit
    Click Button  Save
    Page Should Contain  Changes saved

Delete
    Open Action Menu
    Click Link  css=a#plone-contentmenu-actions-delete
    Click Button  Delete
    Page Should Contain  Plone site
