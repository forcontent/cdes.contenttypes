# -*- coding: utf-8 -*-
from cdes.contenttypes.testing import INTEGRATION_TESTING
from plone import api
from plone.app.contenttypes.interfaces import IEvent
from plone.app.textfield.value import RichTextValue
from plone.dexterity.interfaces import IDexterityFTI
from plone.namedfile.file import NamedBlobImage
from zope.component import createObject
from zope.component import queryUtility

import os
import unittest


class ContentTypeTestCase(unittest.TestCase):

    layer = INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']

        with api.env.adopt_roles(['Manager']):
            self.agenda = api.content.create(
                self.portal, 'cdes.Agenda', 'agenda')
            self.event = api.content.create(
                self.agenda, 'Event', 'event')
            self.setup_content_data()

    def setup_content_data(self):
        path = os.path.dirname(__file__)
        image = open(os.path.join(path, 'image.png')).read()
        self.image = NamedBlobImage(image, 'image/png', u'image.png')

    def test_adding(self):
        self.assertTrue(IEvent.providedBy(self.event))

    def test_fti(self):
        fti = queryUtility(IDexterityFTI, name='Event')
        self.assertIsNotNone(fti)

    def test_schema(self):
        fti = queryUtility(IDexterityFTI, name='Event')
        schema = fti.lookupSchema()
        self.assertEqual(schema.getName(), 'plone_0_Event')

    def test_factory(self):
        fti = queryUtility(IDexterityFTI, name='Event')
        factory = fti.factory
        new_object = createObject(factory)
        self.assertTrue(IEvent.providedBy(new_object))

    def test_behavior_image(self):
        self.assertEqual(self.event.image, None)
        self.event.image = self.image
        self.assertIn(self.event.image.filename, 'image.png')

    def test_behavior_event_type(self):
        self.event.event_type = 'Pellentesque viverra consectetur'
        self.assertEqual(self.event.event_type, 'Pellentesque viverra consectetur')


class ContentTypeViewTestCase(unittest.TestCase):

    layer = INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']

        with api.env.adopt_roles(['Manager']):
            self.event = api.content.create(
                self.portal, 'Event', 'event')

    def test_view(self):
        self.event.title = 'Event'
        self.event.description = 'Etiam aliquam massa vitae diam vehicula, sed ornare enim viverra.'
        self.event.location = 'Pellentesque condimentum commodo nisl, a mattis enim tempus vitae.'
        self.event.text = RichTextValue(
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
            'Nulla vulputate pellentesque sapien, a pretium lectus facilisis eu.',
            'text/plain',
            'text/html'
        )
        self.event.event_type = 'Pellentesque viverra consectetur arcu molestie faucibus.'

        view = self.event.restrictedTraverse('@@view')

        self.assertTrue(view())
        self.assertEqual(view.request.response.status, 200)
        self.assertTrue('Event' in view())
        self.assertTrue('Etiam aliquam massa vitae diam vehicula, sed ornare enim viverra.' in view())
