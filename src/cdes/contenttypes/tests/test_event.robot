*** Settings ***

Resource  plone/app/robotframework/keywords.robot
Variables  plone/app/testing/interfaces.py
Variables  cdes/contenttypes/tests/variables.py
Library  Remote  ${PLONE_URL}/RobotRemote

Test Setup  Open Test Browser
Test Teardown  Close All Browsers

*** Variables ***

${title_selector} =  input#form-widgets-IDublinCore-title
${description_selector} =  textarea#form-widgets-IDublinCore-description
${start_day_selector} =  input#form-widgets-start_date-day
${start_month_selector} =  select#form-widgets-start_date-month
${start_year_selector} =  input#form-widgets-start_date-year
${start_hour_selector} =  input#form-widgets-start_date-hour
${start_min_selector} =  input#form-widgets-start_date-min
${end_day_selector} =  input#form-widgets-end_date-day
${end_month_selector} =  select#form-widgets-end_date-month
${end_year_selector} =  input#form-widgets-end_date-year
${end_hour_selector} =  input#form-widgets-end_date-hour
${end_min_selector} =  input#form-widgets-end_date-min
${attendees_selector} =  textarea#form-widgets-attendees
${event_type_selector} =  input#form-widgets-IExtraFieldEvent-event_type
${image_input_selector} =  input#form-widgets-IExtraFieldEvent-image-input
${alt_image_selector} =  input#form-widgets-IExtraFieldEvent-image_description

*** Test cases ***

Test CRUD
    Enable Autologin as  Site Administrator
    Goto Homepage

    Create Agenda
    Create Event
    Update
    Delete

*** Keywords ***

Click Add Meeting Agenda
    Open Add New Menu
    Click Link  css=a#cdes-agenda
    Page Should Contain  Add Meeting Agenda

Create Agenda
    Click Add Meeting Agenda
    Input Text  css=${title_selector}  Agenda
    Input Text  css=${description_selector}  A agenda
    Click Button  Save
    Page Should Contain  Item created
    Page Should Contain  Agenda

Click Add Event
    Open Add New Menu
    Click Link  css=a#event
    Page Should Contain  Add Event

Create Event
    Click Add Event
    Input Text  css=${title_selector}  Event
    Input Text  css=${description_selector}  A event
    Input Text  css=${start_day_selector}  02
    Input Text  css=${start_year_selector}  2017
    Select From List  css=${start_month_selector}  8
    Input Text  css=${start_hour_selector}  08
    Input Text  css=${start_min_selector}  30
    Input Text  css=${end_day_selector}  02
    Input Text  css=${end_year_selector}  2017
    Select From List  css=${end_month_selector}  8
    Input Text  css=${end_hour_selector}  10
    Input Text  css=${end_min_selector}  00
    Input Text  css=${attendees_selector}  Developers
    Input Text  css=${event_type_selector}  Event type
    Click Button  Save
    Page Should Contain  Item created
    Page Should Contain  Event

Update
    [arguments]

    Click Link  link=Edit
    Input Text  css=${start_hour_selector}  09
    Input Text  css=${start_min_selector}  30
    Choose File  css=${image_input_selector}  ${PATH_TO_TEST_FILES}/image.png
    Input Text  css=${alt_image_selector}  Image description
    Click Button  Save
    Page Should Contain  Changes saved

Delete
    Open Action Menu
    Click Link  css=a#plone-contentmenu-actions-delete
    Click Button  Delete
    Page Should Contain  Plone site
