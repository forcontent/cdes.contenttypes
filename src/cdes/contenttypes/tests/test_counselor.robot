*** Settings ***

Resource  plone/app/robotframework/keywords.robot
Variables  plone/app/testing/interfaces.py
Library  Remote  ${PLONE_URL}/RobotRemote

Test Setup  Open Test Browser
Test Teardown  Close All Browsers

*** Variables ***

${given_name_selector} =  input#form-widgets-given_name
${surname_selector} =  input#form-widgets-surname

*** Test cases ***

Test CRUD
    Enable Autologin as  Site Administrator
    Goto Homepage

    Create
    Update
    Delete

*** Keywords ***

Click Add Counselor
    Open Add New Menu
    Click Link  css=a#counselor
    Page Should Contain  Add Counselor

Create
    Click Add Counselor
    Input Text  css=${given_name_selector}  John
    Input Text  css=${surname_selector}  Doe
    Click Button  Save
    Page Should Contain  Item created
    Page Should Contain  John Doe

Update
    [arguments]

    Click Link  link=Edit
    Click Button  Save
    Page Should Contain  Changes saved

Delete
    Open Action Menu
    Click Link  css=a#plone-contentmenu-actions-delete
    Click Button  Delete
    Page Should Contain  Plone site
