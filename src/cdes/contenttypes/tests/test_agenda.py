# -*- coding: utf-8 -*-
from cdes.contenttypes.interfaces import IAgenda
from cdes.contenttypes.testing import INTEGRATION_TESTING
from plone import api
from plone.dexterity.interfaces import IDexterityFTI
from zope.component import createObject
from zope.component import queryUtility

import unittest


class ContentTypeTestCase(unittest.TestCase):

    layer = INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']

        with api.env.adopt_roles(['Manager']):
            self.agenda = api.content.create(
                self.portal, 'cdes.Agenda', 'agenda')

    def test_adding(self):
        self.assertTrue(IAgenda.providedBy(self.agenda))

    def test_fti(self):
        fti = queryUtility(IDexterityFTI, name='cdes.Agenda')
        self.assertIsNotNone(fti)

    def test_schema(self):
        fti = queryUtility(IDexterityFTI, name='cdes.Agenda')
        schema = fti.lookupSchema()
        self.assertEqual(IAgenda, schema)

    def test_factory(self):
        fti = queryUtility(IDexterityFTI, name='cdes.Agenda')
        factory = fti.factory
        new_object = createObject(factory)
        self.assertTrue(IAgenda.providedBy(new_object))
