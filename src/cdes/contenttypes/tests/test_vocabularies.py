# -*- coding: utf-8 -*-
from cdes.contenttypes.interfaces import ICDESSettings
from cdes.contenttypes.testing import INTEGRATION_TESTING
from plone.registry.interfaces import IRegistry
from zope.component import getUtility
from zope.component import queryUtility
from zope.schema.interfaces import IVocabularyFactory

import unittest


class VocabulariesTestCase(unittest.TestCase):

    layer = INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        registry = getUtility(IRegistry)
        self.settings = registry.forInterface(ICDESSettings)

    def test_activities_vocabulary(self):
        name = 'cdes.contenttypes.Activities'
        util = queryUtility(IVocabularyFactory, name)
        self.assertIsNotNone(util, None)
        activities = util(self.portal)
        self.assertEqual(len(activities), 7)

    def test_document_types_vocabulary(self):
        name = 'cdes.contenttypes.DocumentTypes'
        util = queryUtility(IVocabularyFactory, name)
        self.assertIsNotNone(util, None)
        document_types = util(self.portal)
        self.assertEqual(len(document_types), 7)
