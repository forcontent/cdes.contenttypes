# -*- coding: utf-8 -*-
from cdes.contenttypes.interfaces import ICounselor
from cdes.contenttypes.testing import INTEGRATION_TESTING
from plone import api
from plone.dexterity.interfaces import IDexterityFTI
from zope.component import createObject
from zope.component import queryUtility

import unittest


class ContentTypeTestCase(unittest.TestCase):

    layer = INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']

        with api.env.adopt_roles(['Manager']):
            self.counselor = api.content.create(
                self.portal, 'Counselor', 'counselor')

    def test_adding(self):
        self.assertTrue(ICounselor.providedBy(self.counselor))

    def test_fti(self):
        fti = queryUtility(IDexterityFTI, name='Counselor')
        self.assertIsNotNone(fti)

    def test_schema(self):
        fti = queryUtility(IDexterityFTI, name='Counselor')
        schema = fti.lookupSchema()
        self.assertEqual(ICounselor, schema)

    def test_factory(self):
        fti = queryUtility(IDexterityFTI, name='Counselor')
        factory = fti.factory
        new_object = createObject(factory)
        self.assertTrue(ICounselor.providedBy(new_object))

    def test_title(self):
        self.counselor.given_name = 'John'
        self.counselor.surname = 'Doe'
        self.assertEqual(self.counselor.title, 'John Doe')

    def test_description(self):
        self.assertEqual(self.counselor.description, '')
