# -*- coding: utf-8 -*-
from setuptools import find_packages
from setuptools import setup

version = '1.0b1'
description = 'Content types for CDES site'
long_description = (
    open('README.rst').read() + '\n' +
    open('CONTRIBUTORS.rst').read() + '\n' +
    open('CHANGES.rst').read()
)

setup(
    name='cdes.contenttypes',
    version=version,
    description=description,
    long_description=long_description,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'Framework :: Plone',
        'Framework :: Plone :: 4.3',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: GNU General Public License v2 (GPLv2)',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    keywords='',
    author='forContent',
    author_email='produts@forcontent.com.br',
    url='https://bitbucket.org/forcontent/cdes.contenttypes',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    namespace_packages=['cdes'],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'plone.api',
        'plone.app.content',
        'plone.app.contenttypes',
        'plone.app.dexterity',
        'plone.app.registry',
        'plone.app.textfield',
        'plone.autoform',
        'plone.behavior',
        'plone.dexterity',
        'plone.i18n',
        'plone.indexer',
        'plone.namedfile',
        'plone.registry',
        'plone.supermodel',
        'plone.uuid',
        'Products.CMFPlone >=4.3',
        'Products.GenericSetup',
        'setuptools',
        'z3c.unconfigure',
        'zope.component',
        'zope.i18nmessageid',
        'zope.interface',
        'zope.schema',
    ],
    extras_require={
        'test': [
            'AccessControl',
            'plone.api',
            'plone.app.robotframework',
            'plone.app.testing [robot]',
            'plone.browserlayer',
            'plone.testing',
            'robotsuite',
            'zope.component',
        ],
    },
    entry_points="""
    [z3c.autoinclude.plugin]
    target = plone
    """,
)
