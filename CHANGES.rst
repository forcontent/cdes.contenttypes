Changelog
=========

1.0b1 (unreleased)
------------------

- Add control panel configlet to allow changes on the list of activities and document types;
  a new permission allows this to be edited by Managers and Site Administrators.
  [hvelarde]


1.0a3 (unreleased)
------------------

- Created behavior for Event content type.
  [claytonc.sousa]

- Created cdes.Agenda content type.
  [claytonc.sousa]


1.0a2 (unreleased)
------------------

- Add document classification behavior and associated catalog indexes and metadata.
  You must reinstall the product in order to update the catalog.
  [hvelarde]


1.0a1 (unreleased)
------------------

- Initial release.
